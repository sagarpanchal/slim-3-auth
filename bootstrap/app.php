<?php

session_start();

require_once __DIR__ . '/../vendor/autoload.php';

$config = require '../config.php';
// $config = json_decode(file_get_contents('../config.json'), true);

if ($config['dev']) {
    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);
    $config['settings']['displayErrorDetails'] = true;
    $config['twig']['cache'] = false;
}

# Slim
$app = new \Slim\App([
    'settings' => $config['settings']
]);
$container = $app->getContainer();

# Eloquent
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['database']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['database'] = function ($container) use ($capsule) {
    return $capsule;
};

# Other
$config['classes'] = [
    'flash' => '\\Slim\\Flash\\Messages',
    'auth' => '\\App\\Auth\\Auth'
];
foreach ($config['classes'] as $name => $namespace) {
    $container[$name] = function () use ($namespace) {
        return new $namespace;
    };
}

# Twig
$container['view'] = function ($container) use ($config) {
    $view = new \Slim\Views\Twig($config['twig']['views'], [
        'cache' => $config['twig']['cache'],
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

    $view->addExtension(new \Stormiix\Twig\Extension\MixExtension('../public/assets'));

    $view->getEnvironment()->addGlobal('auth', [
        'check' => $container->auth->check(),
        'user' => $container->auth->user()
    ]);

    $view->getEnvironment()->addGlobal('flash', $container->flash);

    return $view;
};

# Swift Mailer
$container['mailer'] = function ($container) {
    $mailer = new \Anddye\Mailer\Mailer($container['view'], [
        'protocol' => $config['mailer']['protocol'],
        'host' => $config['mailer']['host'],
        'port' => $config['mailer']['port'],
        'username' => $config['mailer']['username'],
        'password' => $config['mailer']['password']
    ]);

    $mailer->setDefaultFrom($config['mailer']['sender_addr'], $config['mailer']['sender_name']);

    return $mailer;
};

# Validator
$container['validator'] = function ($container) {
    return new \App\Validation\Validator;
};

# Controllers
foreach ($config['controllers'] as $controller => $namespace) {
    $container[$controller] = function ($container) use ($namespace) {
        return new $namespace($container);
    };
}

# CSRF
$container['csrf'] = function ($container) {
    return new \Slim\Csrf\Guard;
};

# Middleware
foreach ($config['middleware'] as $middleware) {
    $app->add(new $middleware($container));
}

# Add CSRF
$app->add($container->csrf);

# Custom validator rules
\Respect\Validation\Validator::with('App\\Validation\\Rules\\');

unset($config);

# Routes
require_once __DIR__ . '/../app/routes.php';
