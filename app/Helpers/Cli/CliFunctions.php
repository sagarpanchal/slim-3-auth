<?php

namespace App\Helpers\Cli;

class CliFunctions
{
    /**
     * Print an array in a tree format
     *
     * @param array $input
     * @param string $title
     * @param integer $level
     * @return void
     */
    public function arrayTree(array $input, string $title = '', int $level = 0)
    {
        echo "\e[1;32m", $title, "[]\e[0m \e[2m(", count($input), ")\e[0m\n";
        self::printTree($input);
    }

    private static $printTreePrefix = "";
    private static $printTreeCount = 1;
    private static function printTree($input, $level = 0)
    {
        self::$printTreeCount++;
        foreach ($input as $key => $value) {
            $keys = array_keys($input);
            if ($key === end($keys)) {
                $branch = "└── ";
                $prefixPrefix = "    ";
            } else {
                $branch = "├── ";
                $prefixPrefix = "│   ";
            }
            if (is_array($value)) {
                echo self::$printTreePrefix, $branch, "\e[1;32m", $key, "[] \e[2m(", count($value), ")\e[0m\n";
                if ($level == 0 || ($level > 0 && self::$printTreeCount <= $level)) {
                    $old_prefix = self::$printTreePrefix;
                    self::$printTreePrefix = self::$printTreePrefix . $prefixPrefix;
                    self::printTree($value, $level);
                    self::$printTreePrefix = $old_prefix;
                }
            } else {
                echo self::$printTreePrefix, $branch, "\e[1;33m", $key;
                echo "\e[1;36m ", var_export($value), "\e[0m \e[2m", gettype($value), "(", strlen($value), ")\e[0m\n";
            }
        }
        self::$printTreeCount--;
    }
}
