<?php

namespace App\Helpers;

class Functions
{
    /**
     * Search for Classes in a specified namespace
     *
     * @param string $path
     * @param string $baseNamespace
     * @param array $exclude
     * @return void
     */
    public static function findClasses(string $path, string $baseNamespace, array $exclude = [])
    {
        $allFiles = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        $phpFiles = new \RegexIterator($allFiles, '/\.php$/');
        $fqcns = [];
        foreach ($phpFiles as $phpFile) {
            if (in_array($phpFile->getFileName(), $exclude)) {
                continue;
            }
            $content = file_get_contents($phpFile->getRealPath());
            $tokens = token_get_all($content);
            $namespace = '\\';
            for ($index = 0; isset($tokens[$index]); $index++) {
                if (!isset($tokens[$index][0])) {
                    continue;
                }
                if (T_NAMESPACE === $tokens[$index][0]) {
                    $index += 2; // Skip namespace keyword and whitespace
                    while (isset($tokens[$index]) && is_array($tokens[$index])) {
                        $namespace .= $tokens[$index++][1];
                    }
                    if (strpos($namespace, $baseNamespace) !== 0) {
                        continue 2;
                    }
                }
                if (T_CLASS === $tokens[$index][0] &&
                    T_WHITESPACE === $tokens[$index + 1][0] &&
                    T_STRING === $tokens[$index + 2][0]) {
                    $index += 2; // Skip class keyword and whitespace
                    $fqcns[$tokens[$index][1]] = $namespace.'\\'.$tokens[$index][1];

                    # break if you have one class per file (psr-4 compliant)
                    # otherwise you'll need to handle class constants (Foo::class)
                    break;
                }
            }
        }
        return $fqcns;
    }


    /**
     * Generate a random string, using a cryptographically secure pseudorandom number generator (random_int)
     *
     * @param int $length      How many characters do we want?
     * @param string $keyspace A string of all possible characters to select from
     * @return string
     */
    public static function randomStr($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    /**
     * var_export with php 5.4 array syntax
     *
     * @param [type] $expression
     * @param boolean $return
     * @return void
     */
    public static function varExport($expression, $return = false)
    {
        $export = var_export($expression, true);
        $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
        $array = preg_split("/\r\n|\n|\r/", $export);
        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [null, ']$1', ' => ['], $array);
        $export = join(PHP_EOL, array_filter(["["] + $array));
        if ((bool)$return) {
            return $export;
        } else {
            echo $export;
        }
    }
}
