<?php

namespace App\Auth;

use App\Models\User;
use Respect\Validation\Validator as v;

class Auth
{
    public function userExists($input)
    {
        if (v::email()->validate($input)) {
            return User::where('email', $input)->exists();
        }
        if (v::numeric()->positive()->validate($input)) {
            return User::where('id', $input)->exists();
        }
        return false;
    }

    public function login($email, $password)
    {
        $user = User::select("id", 'password')->where('email', $email)->first();
        if (password_verify($password, $user->password)) {
            $_SESSION['user'] = $user->id;
            return true;
        }

        return false;
    }

    public function check()
    {
        if (isset($_SESSION['user'])) {
            return self::userExists($_SESSION['user']);
        } else {
            return false;
        }
    }

    public function isVerified($input)
    {
        if (v::email()->validate($input)) {
            $verified = User::where('email', $input)->pluck('verified')->first();
        }
        if (v::numeric()->positive()->validate($input)) {
            $verified = User::where('id', $input)->pluck('verified')->first();
        }
        if (!isset($verified)) {
            return false;
        }
        if ($verified == 1) {
            return true;
        }
        return false;
    }

    public function user()
    {
        if (self::check()) {
            return User::find($_SESSION['user']);
        }
        return false;
    }

    public function logout()
    {
        unset($_SESSION['user']);
    }
}
