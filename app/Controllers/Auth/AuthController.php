<?php

namespace App\Controllers\Auth;

use App\Controllers\Controller;
use App\Models\User;
use Respect\Validation\Validator as v;
use App\Helpers\Functions as Fn;

class AuthController extends Controller
{
    public function getSignUp($request, $response)
    {
        return $this->view->render($response, 'auth/signup.twig');
    }

    public function postSignUp($request, $response)
    {
        // Validate request
        $validation = $this->validator->validate($request, [
            'email' => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
            'name' => v::notEmpty()->alpha(),
            'password' => v::noWhitespace()->notEmpty()
        ]);
        if ($validation->failed()) {
            $this->flash->addMessage('error', 'Couldn\'t sign up.');
            return $response->withRedirect($this->router->pathFor('auth.signup'));
        }

        // Register user
        User::create([
            'email' => $request->getParam('email'),
            'name' => $request->getParam('name'),
            'password' => password_hash($request->getParam('password'), PASSWORD_BCRYPT),
            'verified' => Fn::randomStr(32)
        ]);

        if ($this->auth->userExists($request->getParam('email'))) {
            $message = "You have been signed up!"
                ."<br> Check your inbox to verifiy your email address.";
            $this->flash->addMessage('info', $message);
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        $message = "Something went wrong!";
        $this->flash->addMessage('error', $message);
        return $response->withRedirect($this->router->pathFor('auth.signin'));
    }

    public function getSignIn($request, $response)
    {
        return $this->view->render($response, 'auth/signin.twig');
    }

    public function postSignIn($request, $response)
    {
        // Validate request
        $validation = $this->validator->validate($request, [
            'email' => v::noWhitespace()->notEmpty()->email(),
            'password' => v::noWhitespace()->notEmpty()
        ]);
        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        // Check if email is registered
        if (!$this->auth->userExists($request->getParam('email'))) {
            $message = "The email address that you've entered doesn't match any account.";
            $this->flash->addMessage('error', $message);
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        // Check if password is correct
        $password = User::where('email', $request->getParam('email'))->pluck('password')->first();
        if (!password_verify($request->getParam('password'), $password)) {
            $this->flash->addMessage('error', "The password that you've entered is incorrect.");
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        // Check if email is verified
        $verified = $this->auth->isVerified(
            $request->getParam('email')
        );
        if (!$verified) {
            $message = "Check your inbox to verify your email address."
                ."<br>You'll need to verify your email address to activate your account.";
            $this->flash->addMessage('error', $message);
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        // Login if email and password match and email is verified
        $login = $this->auth->login(
            $request->getParam('email'),
            $request->getParam('password')
        );
        if (!$login) {
            $this->flash->addMessage('error', "Something went wrong!");
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->router->pathFor('home'));
    }

    public function getSignOut($request, $response)
    {
        $this->auth->logout();
        return $response->withRedirect($this->router->pathFor('auth.signin'));
    }

    public function getChangePassword($request, $response)
    {
        $this->view->render($response, "auth/password/change.twig");
    }

    public function postChangePassword($request, $response)
    {
        $validation = $this->validator->validate($request, [
            'password_old' => v::notEmpty()->matchesPassword($this->auth->user()->password),
            'password' => v::noWhitespace()->notEmpty()
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor('auth.password.change'));
        }

        $this->auth->user()->setPassword($request->getParam('password'));

        $this->auth->logout();

        $this->container->flash->addMessage('info', 'Password Changed.');

        return $response->withRedirect($this->router->pathFor('auth.signin'));
    }
}
