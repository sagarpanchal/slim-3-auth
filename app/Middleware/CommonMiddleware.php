<?php

namespace App\Middleware;

class CommonMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if (isset($_SESSION['user'])) {
            if (!$this->container->auth->userExists($_SESSION['user'])) {
                $this->container->auth->logout();
            }
        }

        $response = $next($request, $response);
        return $response;
    }
}
