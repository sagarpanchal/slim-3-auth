<?php

namespace App\Middleware;

class AuthMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if (!$this->container->auth->check()) {
            throw new \Slim\Exception\NotFoundException($request, $response);
            // $message = "The link you followed may be broken, or the page may have been removed.";
            // $this->container->flash->addMessage('error', $message);
            // return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $response = $next($request, $response);
        return $response;
    }
}
