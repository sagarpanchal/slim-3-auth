document.addEventListener("DOMContentLoaded", function() {

  // add active class to nav-link to current page
  navLink = document.getElementsByClassName("nav-link");
  for (var i = 0; i < navLink.length; i++) {
    if (navLink.item(i).getAttribute('href') === window.location.pathname) {
      navLink.item(i).classList.add('active');
    }
  }

  alerts.forEach(function(alert) {
    $.notify({
      message: _.unescape(alert.message)
    }, {
      offset: {
        y: 3
      },
      delay: 0,
      type: alert.type,
    });
  });

});
