const mx = require("laravel-mix");

mx.setPublicPath('public/assets');

// Generate source maps when not in production
if (!mx.inProduction()) {
  mx.webpackConfig({devtool: "source-map"})
    .sourceMaps()
}

// Cache busting
mx.version();

// Copy fontawesome assets
mx.copy("node_modules/@fortawesome/fontawesome-free/webfonts", "public/assets/fonts")

// Compile
mx.js("resources/js/app.js", "public/assets/js")
  .sass("resources/sass/app.scss", "public/assets/css");
