# Slim 3 Auth
Slim 3 with basic MVC setup and user authentication with middleware.

## Setup Instrutions

### Prerequisites
Run following commands in project root to install dependencies
```
composer update
```
```
npm install
```

### MariaDB setup (works with MySQL)
Look for ```database.sql``` in project root, it contains MySQL queries for databse setup.

### Configuration
Add database name and credentials inside ```config.default.php```.
```
    'settings' => [
        'database' => [
            'database' => '<database_name>',
            'username' => '<database_username>',
            'password' => '<database_password>',
```

Add email credentials inside ```config.default.php```.
```
    'mailer' =>  [
        'protocol' => '<protocol>',
        'host' => '<host>',
        'port' => '<port>',
        'username' => '<username>',
        'password' => '<password>',
        'sender_addr' => '<sender_email>',
        'sender_name' => '<sender_name>',
    ],
```

Remove  ```config.php``` if exists, and then run ```./slim update all``` to create new ```config.php```.

You can also run ```./slim show config``` to see current configuration.

Addiotionally, you can run ```./slim update <all|controllers|middleware>``` to automatically regiter new controller and/or middleware classes.

### Webpack
Laravel Mix provides a fluent API for defining Webpack build steps for your application using several common CSS and JavaScript pre-processors.
To compile webpack assets run ```npm run development``` (not minified) or ```npm run production``` (minified).

## Built With
- [Slim Framework v3](https://www.slimframework.com/docs/)
- [Eloquent ORM](https://laravel.com/docs/4.2/eloquent)
- [Laravel Mix](https://laravel.com/docs/5.7/mix#running-mix)
