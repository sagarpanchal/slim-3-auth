<?php
return [
    'dev' => true,
    'settings' => [
        'displayErrorDetails' => false,
        'database' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'ex_slim_auth',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ],
    ],
    'twig' => [
        'views' => '../resources/views/',
        'cache' => '../cache/twig/',
    ],
    'mailer' =>  [
        'protocol' => 'smtp',
        'host' => 'smtp.mailtrap.io',
        'port' => '2525',
        'username' => '',
        'password' => '',
        'sender_addr' => 'no-reply@mail.com',
        'sender_name' => 'Webmaster',
    ],
];
